/*************************************************************************//**
@file       LoggerUserType1.cpp

@brief      An example of a log client.
@details    This client posts a message at regular increments while cycling
            through the log levels. 

*****************************************************************************/
// Standard Includes
#include <thread>
#include <chrono>

// Project Includes
#include "ThreadMessage.h"
#include "LoggerService.h"
#include "LoggerBaseUser.h"
#include "LoggerUserType1.h"

/**
 * @brief      Default constructor
 */
LoggerUserType1::LoggerUserType1()
{
	_log = &LoggerService::instance();
	_msg._msgPayload._clientID = "Type1-Default";
	_counter = 0;
	_msgCounter = 0;
}

/**
 * @brief      Default destructor
 */
LoggerUserType1::~LoggerUserType1()
{
	StopUser();
}

/**
 * @brief      Calling this function starts the user thread
 *
 * @param[in]  None.
 *
 * @return     True if the thread started, False otherwise
 */
bool LoggerUserType1::StartUser()
{
	return LoggerBaseUser::StartUser(std::bind(&LoggerUserType1::Proccess, this));
}

/**
 * @brief      The function that will be accessed by the thread
 *
 * @param[in]  None.
 *
 * @return     None.
 */
void LoggerUserType1::Proccess()
{
	// Define the type of request we are going to make
	_msg._msgHeader = LOG_MESSAGE;

	while (_running)
	{
		_msg._msgPayload._logLevel = (LogLevel) _counter;
		_msg._msgPayload._message = "Message : " + std::to_string(_msgCounter);
		_log->PostMessage(_msg);

		_counter = (_counter + 1) % 3;
		_msgCounter++;

		std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
}