/*************************************************************************//**
@file       LoggerBaseUser.cpp

@brief      A base class for users of the logging service
@details    A class which can be used as a building block for logger clients

*****************************************************************************/
// Project Includes
#include "LoggerBaseUser.h"
#include "LoggerService.h"
#include "ThreadMessage.h"

/**
 * @brief      Default constructor
 */
LoggerBaseUser::LoggerBaseUser()
{
	_running = false;
	_threadPtr = nullptr;
	_log = nullptr;
}

/**
 * @brief      Default destructor 
 */
LoggerBaseUser::~LoggerBaseUser()
{
	StopUser();
}

/**
 * @brief      Calling this function starts the user thread
 *
 * @param[in]  fxn: A function which will be called by the thread.
 *
 * @return     True if the thread started, False otherwise
 */
bool LoggerBaseUser::StartUser(std::function<void()> fxn)
{
	if (_threadPtr == nullptr)
	{
		_running = true;
		_threadPtr = new std::thread(fxn);
		return true;
	}
	return false;
}

/**
 * @brief      Calling this function stops the user thread
 *
 * @param[in]  None.
 *
 * @return     True if the thread stopped, False otherwise
 */
bool LoggerBaseUser::StopUser()
{
	if (_threadPtr != nullptr)
	{
		_running = false;
		_threadPtr->join();
		delete _threadPtr;
		_threadPtr = nullptr;
		return true;
	}
	return false;
}

/**
 * @brief      Sets the client ID
 *
 * @param[in]  _name : The new client ID
 *
 * @return     None.
 */
void LoggerBaseUser::setClientID(const std::string& _name)
{
	_msg._msgPayload._clientID = _name;
}