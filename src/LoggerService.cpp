/*************************************************************************//**
@file       LoggerService.cpp

@brief      A logging service, the main include for this project.
@details    A logging service that is designed to work in a multi-threaded
			program. It can support multiple users at a time.

*****************************************************************************/

// Project Includes
#include "LoggerService.h"

/**
 * @brief      Default constructor, unreachable because this class is a singleton
 */
LoggerService::LoggerService()
{
	_running = false;
	_messageQueue = new std::queue<ThreadMsg>; 
	_logBuffer = new std::vector<LogMsg>;
	_threadPtr = nullptr;
}

/**
 * @brief      Default destructor
 */
LoggerService::~LoggerService()
{
	delete _messageQueue;
}

/**
 * @brief      Class access point
 *
 * @param[in]  None.
 *
 * @return     Returns the instance of the logging class.
 */
LoggerService& LoggerService::instance()
{
	static LoggerService INSTANCE;
	return INSTANCE;
}

/**
 * @brief      Sends a message to the logger to be processed.
 *
 * @param[in]  _msg the message you want to post to the logger.
 *
 * @return     None.
 */
void LoggerService::PostMessage(const ThreadMsg & _msg)
{
	if (_threadPtr != nullptr && _running)
	{
		// Post the message to the queue so it can be taken care of
		std::unique_lock<std::mutex> uLock (_messageQueueLock);
		_messageQueue->push(_msg);
		_cv.notify_one();
	}
}

/**
 * @brief      Calling this function stops the user thread
 *
 * @param[in]  None.
 *
 * @return     True if the thread stopped, False otherwise
 */
bool LoggerService::StopService()
{
	if (_threadPtr != nullptr)
	{
		// Set the condition to be false to stop the loop
		_running = false;
		// Notify the task in case it was waiting for it
		_cv.notify_one();
		// Join with the thread
		_threadPtr->join();
		// Clean up the memory
		delete _threadPtr;
		_threadPtr = nullptr;
		return true;
	}
	return false;
}

/**
 * @brief      Calling this function starts the user thread
 *
 * @param[in]  None.
 *
 * @return     True if the thread started, False otherwise
 */
bool LoggerService::StartService()
{
	if (_threadPtr == nullptr)
	{
		_running = true;
		// Grab the lock for the Log buffer
		std::unique_lock<std::mutex> uLockLog(_LogLock);
		_logBuffer->clear();
		// Start the thread
		_threadPtr = new std::thread(&LoggerService::Proccess, this);
		return true;
	}
	return false;
}

// A function to get a human-readable version of the log levels
std::string LogLevelToString(LogLevel _level)
{
	switch (_level)
	{
	case INFO:
		return  "INFO ";
	case WARNING:
		return  "WARN ";
	case ERROR:
		return  "ERROR";
	}
	return "UNKOWN";
}

void LoggerService::HandleMessage(const ThreadMsg _msg)
{
	// Grab the lock for the Log buffer
	std::unique_lock<std::mutex> uLockLog(_LogLock);
	LogLevel _level = _msg._msgPayload._logLevel;

	switch (_msg._msgHeader)
	{
	case LOG_MESSAGE:
		// All we need to do is push the message to the log
		_logBuffer->push_back(_msg._msgPayload);
		break;
	case  DUMP_LOG:
		for (size_t i = 0; i < _logBuffer->size(); i++)
		{
			if (_logBuffer->at(i)._logLevel >= _level)
			{
				std::cout << "<" << LogLevelToString(_logBuffer->at(i)._logLevel) << "> "
					<< _logBuffer->at(i)._clientID << " : "
					<< _logBuffer->at(i)._message << "\n";
			}
		}
		break;
	case  CLEAR_LOG:
		_logBuffer->clear();
		break;
	default:
		break;
	}
}

/**
 * @brief      The function that will be accessed by the thread
 *
 * @param[in]  None.
 *
 * @return     None.
 */
void LoggerService::Proccess()
{
	while (_running)
	{
		// Acquire the message buffer lock
		std::unique_lock<std::mutex> uLock (_messageQueueLock);
		
		// While we don't have a message wait to be notified
		while (_messageQueue->empty() && _running) _cv.wait(uLock);

		// If the queue is empty there isn't much we can do
		if (_messageQueue->empty()) continue;

		// Get the message
		ThreadMsg _msg = _messageQueue->front();
		_messageQueue->pop();

		// We are done with the lock, we can release it
		uLock.unlock(); 

		// Handle the message we just got
		HandleMessage(_msg);
	}

	// If we got here, then someone has asked for the service to stop before we leave though
	// we should process all the pending messages
	// Acquire the message buffer lock, to prevent anyone add more messages
	std::unique_lock<std::mutex> uLock(_messageQueueLock);
	while (!_messageQueue->empty())
	{
		// Get the message
		ThreadMsg _msg = _messageQueue->front();
		_messageQueue->pop();

		// Handle the message
		HandleMessage(_msg);
	}
}