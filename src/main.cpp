/*************************************************************************//**
@file       main.cpp

@brief      The main entry point for this project.
@details    This main is mostly meant to show the loggers functionally, the
            is meant to be used as a single include.

*****************************************************************************/
// Standard Includes
#include <iostream>
#include <chrono>
#include <thread>

// Project Includes
#include "ThreadMessage.h"
#include "LoggerService.h"
#include "LoggerBaseUser.h"
#include "LoggerUserType1.h"

void PrintTestStart(int testNumber, std::string testName)
{
	std::cout << "****************************************************************************************\n";
	std::cout << "Starting Test # " << testNumber << " - " << testName << "\n";
	std::cout << "----------------------------------------------------------------------------------------\n";
}

void PrintTestStop(int testNumber, std::string testName)
{
	std::cout << "----------------------------------------------------------------------------------------\n";
	std::cout << "Ending Test # " << testNumber << " - " << testName << "\n";
	std::cout << "****************************************************************************************\n";
}

void PrintTestResult(std::string testName, bool result)
{
	std::cout << testName << " - " << ((result) ? "Passed" : "Failed") << "\n";
}

void DumpLog(LoggerService* _logger, LogLevel _level)
{
	ThreadMsg _msg;
	_msg._msgPayload._clientID = "Main";
	_msg._msgPayload._logLevel = _level;
	_msg._msgHeader = DUMP_LOG;
	_logger->PostMessage(_msg);
}

int main()
{
	// Get a reference to the logger
	LoggerService* _logger = &LoggerService::instance();

	// Unit test #1 - Using the logger without starting the service
	PrintTestStart(1, "Using service without starting it");
	{
		// Create a user
		LoggerUserType1* _user1 = new LoggerUserType1;
		_user1->setClientID("User 1");
		_user1->StartUser();

		// Put this thread to sleep for a little, to allow the user to post some messages
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		// Stop the user
		_user1->StopUser();
		delete _user1;

		// Tell the logger to dump it's logs
		DumpLog(_logger, INFO);
	}
	PrintTestStop(1, "Using service without starting it");

	// Unit test #2 - Starting the service multiple times
	PrintTestStart(2, "Repeated Starts of the service");
	{
		// Start the logging service
		PrintTestResult("Service Started", _logger->StartService());

		for (int i = 0; i <= 100; i++)
		{
			if (_logger->StartService())
			{
				// The test failed
				PrintTestResult("Service Repeated Starts", false);
				break;
			}
			else if (i == 100)
			{
				PrintTestResult("Service Repeated Starts", true);
			}
		}
	}
	PrintTestStop(2, "Repeated Starts of the service");

	// Unit test #3 - Testing dumping different debug levels
	PrintTestStart(3, "Different Debug Levels");
	{
		// Create a user
		LoggerUserType1* _user1 = new LoggerUserType1;
		_user1->setClientID("User 1");
		_user1->StartUser();

		// Put this thread to sleep for a little, to allow the user to post some messages
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		// Stop the user from making more messages
		_user1->StopUser();

		// Tell the logger to dump only the error logs
		std::cout << "Only error logs : \n";
		DumpLog(_logger, ERROR);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Tell the logger to dump only the error and warning logs
		std::cout << "Only error and warning logs : \n";
		DumpLog(_logger, WARNING);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Tell the logger to dump all logs
		std::cout << "All Logs : \n";
		DumpLog(_logger, INFO);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Clean up user
		delete _user1;
	}
	PrintTestStop(3, "Different Debug Levels");

	// Unit test #4 - Many Users
	PrintTestStart(4, "Many Users");
	{
		// Create a user
		LoggerUserType1* _users[5];
		for (int i = 0; i < 5; i++)
		{
			_users[i] = new LoggerUserType1(); 
			_users[i]->setClientID("User " + std::to_string(i));
			_users[i]->StartUser();
		}

		// Put this thread to sleep for a little, to allow the user to post some messages
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		// Stop the users from making more messages
		for (int i = 0; i < 5; i++)
		{
			_users[i]->StopUser();
		}

		// Tell the logger to dump only the error logs
		std::cout << "Only error logs : \n";
		DumpLog(_logger, ERROR);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Tell the logger to dump only the error and warning logs
		std::cout << "Only error and warning logs : \n";
		DumpLog(_logger, WARNING);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Tell the logger to dump all logs
		std::cout << "All Logs : \n";
		DumpLog(_logger, INFO);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout << "\n";

		// Clean up users
		for (int i = 0; i < 5; i++)
		{
			delete _users[i];
		}
	}
	PrintTestStop(4, "Many Users");

	// Stop the service 
	_logger->StopService();

	return 0;
}