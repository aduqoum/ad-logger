# Ad-Logger Readme

The purpose of this project is to develop a logger for multi-thread projects, that is simple to integrate into projects and simple to use.

### Nomenclature 
Before reading this readme, it is a good idea to define some terms so as to disambiguate concepts.

1. **"Logger Service"** or **"Logger"** will be used to define the object who is tasked with managing logs from different clients
2. **"Logger Client"** or **"Client"** or **"Logger User"** will be used to define the object(s) that use the logger and send messages to it
3. **"Message Buffer"** or **"Message Queue"** will be used to describe the way in which threads communicate

# Assumptions and Decisions
There was a number of assumptions made while making this project, the validity and reasons behind them will be explain in this section.

### Service Design
There will only ever be one logger in any system, and as such to avoid confusion and protect the user from themselves we should make the logger server a [singleton](https://en.wikipedia.org/wiki/Singleton_pattern). While some consider it to be an anti-pattern, I thought for this project it would be suitable. 

### User Design
All users can be built from a base user class (although this is not necessary, any class can use the logger). The base class is simply in place to help users of 
this project.

### Message Queues
Because the design of a client is not known (the user should be able to make any kind of client they want) I needed a way to buffer the client from the service. This is where message queues comes in, the basic idea is no one client can alter the logs, but they can make a request to the service to change it. This keeps the logs safe and lets the client work at whatever pace they want.

# Build Instructions
This project was built using **Visual Studio 2019** on a **Windows 10** operating system. No setup steps are necessary as all resources used are either included with the VS build system or are in the project. 
# Limitations and Todo's
The build system for this project is not great, and in fact was made only because time was running short. A better solution would be to use **CMake** to make sure the project is cross-platform and to avoid committing the solution file. 

All threads have the same priority, this was done to save time mostly, on a real project I would enforce some kind of thread prioritization and management. 

The unit tests are all manual right now, and there isn't that many of them. I would replace them with a framework (like **catch**) and add more of them.

# Interaction Diagram
A rough diagram of how classes interact can be found below.
```mermaid
graph LR
A(Service Client) -- Log Msg --> B[Service Queue]
A(Service Client) -- Dump Log --> B[Service Queue]
A(Service Client) -- Clear text --> B[Service Message Queue]
C(Logger Service) -- Get Message --> B[Service Message Queue]
B[Service Message Queue] -- Notify --> C(Logger Service)
C(Logger Service) -- Store Log Message --> D[Log Buffer]
C(Logger Service) -- Clear Log Buffer --> D[Log Buffer]
D[Log Buffer] -- Logs --> C(Logger Service)
C(Logger Service) -- Logs --> E[Output Stream]
```