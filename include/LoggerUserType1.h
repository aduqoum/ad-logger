/*************************************************************************//**
@file       LoggerUserType1.h

@brief      An example of a log client.
@details    This client posts a message at regular increments while cycling
			through the log levels.

*****************************************************************************/

#ifndef LOGGER_USER_TYPE1_H
#define LOGGER_USER_TYPE1_H

// Forward Declare the classes we need
class LoggerBaseUser;

class LoggerUserType1 : public LoggerBaseUser
{
public:
	LoggerUserType1();
	~LoggerUserType1();

	bool StartUser();

private:
	void Proccess() override;

	int _counter;
	int _msgCounter;
};

#endif