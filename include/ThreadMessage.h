/*************************************************************************//**
@file       ThreadMessage.h

@brief      A file that define the structure of messages
@details    None.

*****************************************************************************/

#ifndef THREAD_MESSAGE_H
#define THREAD_MESSAGE_H

#include <string>

// Create an enum for the different log levels
enum LogLevel { INFO, WARNING,  ERROR };

// Create an enum for the different requests a client can make to the main service
enum MesssageHeader{ LOG_MESSAGE,  DUMP_LOG, CLEAR_LOG };

// Define what a log message is
struct LogMsg
{
	std::string _clientID;
	LogLevel _logLevel;
	std::string _message;
};

// Define what a thread message is, this will be used for inter-thread communication
struct ThreadMsg
{
	MesssageHeader _msgHeader;
	LogMsg _msgPayload;
};

#endif // ! THREAD_MESSAGE_H
