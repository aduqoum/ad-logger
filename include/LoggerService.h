/*************************************************************************//**
@file       LoggerService.h

@brief      A logging service, the main include for this project.
@details    A logging service that is designed to work in a multi-threaded
            program. It can support multiple users at a time. 

*****************************************************************************/
#ifndef LOGGER_SERVICE_H
#define LOGGER_SERVICE_H

// Standard Includes
#include <mutex>
#include <thread>
#include <queue>
#include <vector>
#include <iostream>
#include <functional>
#include <atomic>

// Project Includes
#include "ThreadMessage.h"

class LoggerService
{
public:
	void PostMessage(const ThreadMsg& _msg);
	bool StartService();
	bool StopService();
	
	// We want the Service to be a singleton
	static LoggerService& instance();
	LoggerService(const LoggerService&) = delete;
	LoggerService(LoggerService&&) = delete;
	LoggerService& operator=(const LoggerService&) = delete;
	LoggerService& operator=(LoggerService&&) = delete;

private:
	LoggerService();
	~LoggerService();
	void Proccess();
	void HandleMessage(const ThreadMsg _msg);

	std::atomic<bool> _running;
	std::thread* _threadPtr;
	// Message Queue Variables
	std::mutex _messageQueueLock;
	std::queue<ThreadMsg>* _messageQueue;
	std::condition_variable _cv;
	// Log Variables
	std::mutex _LogLock;
	std::vector<LogMsg>* _logBuffer;
};

#endif