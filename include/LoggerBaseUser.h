/*************************************************************************//**
@file       LoggerBaseUser.h

@brief      A base class for users of the logging service
@details    A class which can be used as a building block for logger clients

*****************************************************************************/

#ifndef LOGGER_BASE_USER_H
#define LOGGER_BASE_USER_H

// Standard Includes
#include <thread>
#include <string>
#include <atomic>
#include <functional>

// Project Includes
#include "ThreadMessage.h"

// Forward declare our objects
class LoggerService;

class LoggerBaseUser
{
public:
	LoggerBaseUser();
	~LoggerBaseUser();

	bool StopUser();
	bool StartUser(std::function<void()> fxn);
	void setClientID(const std::string& _name);

protected:
	virtual void Proccess() = 0;
	
	std::atomic<bool> _running;
	LoggerService* _log;
	ThreadMsg _msg;
	std::thread* _threadPtr;
};

#endif